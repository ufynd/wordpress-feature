<?php
/**
 * Some References
 * https://learn.wordpress.org/
 * 
 * - WP Classic Themes:
 *   - Theme Basics: https://developer.wordpress.org/themes/basics/
 *   - Classic Themes: https://developer.wordpress.org/themes/classic-themes/
 * - WP child theme development: 
 *   - https://www.hostinger.com/tutorials/how-to-create-wordpress-child-theme#How_to_Create_a_Child_Theme_in_WordPress
 * - WP APIs
 *   - WP Loop: listing posts:
 *      - https://codex.wordpress.org/The_Loop
 *   - Shortcodes in WP:
 *      - https://www.wpbeginner.com/wp-tutorials/how-to-add-a-shortcode-in-wordpress/
 *      - https://wordpress.tv/2022/07/29/lets-code-common-apis-shortcodes/
 */

/**
 * Pipe the Parent theme styles to this theme
 */
add_action( 'wp_enqueue_scripts', 'enqueue_parent_styles' );
function enqueue_parent_styles() {
   wp_enqueue_style( 'parent-style', get_template_directory_uri().'/style.css' );
}

/**
 * Function that runs when shortcode is called
 */
function render_silly_shortcode() {
    $output = '<div class="silly-shortcode">Theme shortcode</div>';
    return $output;
}
add_shortcode('silly', 'render_silly_shortcode');
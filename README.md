# Wordpress-feature

Boilerplate to start a WordPress project.

Use this project to prepare your work. With this project you will setup a WordPress environment for development using
modern approach.

## Getting started

Installing some required tools. In the end you can see the references links.

1. Install Docker 
   1. Install, and start Docker Desktop following the instructions for your OS:
      1. https://www.docker.com/products/docker-desktop
2. Install Node.js (in case you do not have it)
   1. We would recomend the use of NVM: https://github.com/nvm-sh/nvm

After NVM installation is complete, you can install node version 14
```shell      
nvm install 14
```

3. Install WordPress tools
```shell
npm -g install @wordpress/env
```

### References
- https://developer.wordpress.org/block-editor/getting-started/devenv/

## Start your development

Once all tools are installed, you must (using your terminal) navigate to this repository project and run the command:

```shell
wp-env start
```

This command will start all the WordPress requirements in Docker environment. The command might take a while to run.
Once it is done, you can access the WordPress site on http://localhost:8888/

To manage its content, access: http://localhost:8888/wp-admin

The user admin credentials are:

user: admin / password: password

Create your project, whether a theme or a plugin, inside the respective folder, and it will be available on the 
WordPress installation.

## Extras

To stop your development environment run:
```shell
wp-env stop
```

To see all possible commands available on `wp-env` command, type:
```shell
wp-env --help
```

To erase all WordPress installation data, you can type:
```shell
wp-env destroy
```

## For Candidates

If you are a candidate using this boilerplate to a project, we suggest the following steps:

1. Clone this project in your local machine. In case you have a GitLab account, fork it;
2. Do your work and commits;
3. Change this 'For Candidates' section to 'Development Notes' and write notes about your development process.
